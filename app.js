//declaring and storing the DOM elements' variables
const outStandingValueElement = document.getElementById("outStandingValue");
const valuesElement = document.getElementById("value");
const payElement = document.getElementById("pay");
const myBankElement = document.getElementById("myBank");
const getLoanButtonElement = document.getElementById("getLoanButton").style.color = 'black';
const bankButtonElement = document.getElementById("bankButton").style.color = 'black';
const increment = document.getElementById('increment');
const laptopsElement = document.getElementById("laptops");
const buynowElement = document.getElementById("buynow").style.color = 'black';
const repayLoan = document.getElementById("repayLoan");
let priceElement = document.getElementById("price");
const nameElement = document.getElementById("name");
const descriptionElement = document.getElementById("description");
const specsElement = document.getElementById("specs");
const img_homeElement = document.getElementById("img_home");

//declare some variables with let type with numbers to use in mathematical equations in the logic behind the functionality of each button
let zeroValue = 0;
let pay = 0;
let myBank = 0;
let getLoan = 0;
let restMoney = 0;
//an empty array for storing all computers in it
let laptops = [];

//zero numbers in the layout of the website when the user land in the page for the first time
myBankElement.innerText = myBank;
valuesElement.innerText = zeroValue;
outStandingValueElement.innerText= getLoan;

//this function that increases the "pay" variable, which is the users' salary, by 100 each time the "work" button is pressed
increment.addEventListener('click', addValue);  
function addValue(){
  pay = zeroValue+= 100;
  valuesElement.innerText = pay;
}

//this function handles the functionality of the "Bank" button
//If the user has an outstanding loan, 10% of the salary will be deducted and transferred to the outstanding Loan amount and after the 10% deduction will be transferred to the bank balance 
//if the user has no loan then the whole salary will go to the bank balance
bankButton.addEventListener('click', transferValue); 
function transferValue(){
	if(getLoan > 0){
    myBank += pay * 0.9;
    getLoan += pay *0.1; 
    zeroValue = 0;
    pay= 0;
    valuesElement.innerText = zeroValue; 
    myBankElement.innerText = myBank;
    outStandingValue.innerHTML= getLoan;
	}else if(pay > 0 && getLoan === 0){
		myBank += pay;
    zeroValue = 0;
    pay= 0;
    valuesElement.innerText = zeroValue; 
    myBankElement.innerText = myBank;
	}
} 

//this function handle the "get a loan" button
//this function shows a “Prompt” popup box that allows the user to enter an loan amount after clicking on a "get a loan" button. 
//the user cannot get a loan more than double of the bank balance 
//the user cannot have two loans at once. The initial loan should be paid back in full
let busy = false;
getLoanButton.addEventListener('click', promptMe);
function promptMe(){
  var loan = prompt("Please enter the amount of your loan");
  getLoan = parseInt(loan);
  maxLoan = myBank * 2;

  if(loan === null || loan === "" ){
    return;
  } else if(getLoan > maxLoan && busy === false ){
    alert ("Sorry, You cannot get a loan more than double of your bank balance!")
    getLoan= 0;
    return false;
  }else if (busy === false ){
    outStandingValue.innerHTML= getLoan;
    busy=true;
  }else{
    alert ("You cannot get more than one bank loan before repaying the last loan!")
  }

  if(getLoan > 0){
    repayLoan.style.display = "inline";
  }else{
  repayLoan.style.display = "none";
  }

} 

//this function appears a new "Repay Loan" button once the user get a loan. 
//this function transfer the full value of the users' salary towards the outstanding loan and reduce the outstanding loan value.
//any remaining funds after paying the loan transfers to the users' bank balance
repayLoan.addEventListener('click', payBackLoan);
function payBackLoan(){
  if(pay > getLoan && getLoan != 0){
    restMoney = pay -= getLoan;
    zeroValue = 0;
    pay=0;
    getLoan=0;
    valuesElement.innerText = zeroValue; 
    outStandingValueElement.innerHTML = zeroValue;
    myBankElement.innerText = myBank += restMoney;
    busy=false;
  }else if(pay < getLoan){
    restMoney = getLoan -= pay;
    zeroValue = 0;
    pay=0;
    valuesElement.innerText = zeroValue; 
    outStandingValueElement.innerHTML = restMoney;
    busy=true;
  }else if(pay === getLoan){
    zeroValue = 0;
    valuesElement.innerText = zeroValue; 
    outStandingValueElement.innerHTML = zeroValue;
    busy=false;
  }
} 

//retrieving data from the API
fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
.then(response => response.json()) 
.then(data => laptops = data)
.then(laptops => addLaptopsToMenu(laptops));

//add laptops to the menu and take the first laptops information for the websites' layout
//values such as title, price, specs, description and the image
const addLaptopsToMenu = (laptops) => {
  laptops.forEach(x => addLaptopToMenu(x));
 priceElement= laptops[0].price;
 document.getElementById("price").innerHTML = priceElement;
  specsElement.innerText = laptops[0].specs;
  descriptionElement.innerText = laptops[0].description;
  nameElement.innerText = laptops[0].title;
  img_homeElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + laptops[0].image;
}

//take the information of any clicked computer from API
//get the title of laptops from API and store them in the dropdowns' options
const addLaptopToMenu = (laptop) => {
  const laptopElement = document.createElement("option");
  laptopElement.value = laptop.id;
  laptopElement.appendChild(document.createTextNode(laptop.title));
  laptopsElement.appendChild(laptopElement);
}

//change the information of the page based on the clicked computer
//information such as image, title, description, price and the specs
const handleLaptopMenuChange = e => {
  const selectedLaptop = laptops[e.target.selectedIndex];
  priceElement = selectedLaptop.price;
  document.getElementById("price").innerHTML = priceElement;
  nameElement.innerText = selectedLaptop.title;
  descriptionElement.innerText = selectedLaptop.description;
  specsElement.innerText = selectedLaptop.specs;
  img_homeElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedLaptop.image;
  
}

//the functipn "handleLaptopMenuChange" runs each time a laptop is clicked
laptopsElement.addEventListener("change", handleLaptopMenuChange);

//this function runs when the "buy now" button is clicked
//If the user does not have enough money in the bank balance, a message will appear informing the user that they cannot afford the selected computer.
//when the user has enough bank balance, the amount of the laptop will be deducted from the bank balance and a congrats message as a alert will be shown
function buyIt(){
  if(priceElement > myBank){
    
    alert("Sorry, you cannot afford the laptop!")
    myBankElement.innerText = myBank;
    priceElement.innerText = priceElement;
  }else{

    alert("you are now the owner of the new laptop!")
    myBank = myBank - priceElement;
    console.log(priceElement);
    myBankElement.innerText = myBank;
    priceElement.innerText = priceElement;
  }

}
